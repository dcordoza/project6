import socket
from _thread import *
from datetime import datetime
import time
import json
import mariadb
import sys

currRTU = 0

try:
    conn = mariadb.connect(
        user="root",
        password="red376Car!!",
        host="localhost",
        port=3306,
        database="alarmMaster")

except mariadb.Error as e:
    print(f"Error connecting {e}")
    sys.exit(1)

cur = conn.cursor()

now = (datetime.now()).strftime("%Y-%m-%d %H:%M:%S")
'''
cur.execute("INSERT INTO eventHist (device,timestamp,description,type,ip) VALUES (?, ?, ?, ?, ?)", ("Tower 2", now, "Temp Major Over", "ALM", "1.2.3.4"))
conn.commit()
'''

send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
recieve = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
recieve.bind(("1.2.3.3", 5005))

def intToBytes(i):
    j=hex(i)
    j=j[2:]
    if i < 16:
        j = '0' + j
    k = bytes.fromhex(j)
    return k

def BCH(buff):
    bch = 0x00
    nBCHpoly = 0xB8
    fBCHpoly = 0xFF
    for i in buff:
        bch ^= i
        for j in range(8):
            if (bch & 1) == 1:
                bch = (bch >> 1) ^ nBCHpoly
            else:
                bch >>= 1

    bch ^= fBCHpoly
    return bch

def sendPkt(targetIP, targetPort, targetID):
    print("pkt sent")
    uid = intToBytes(targetID)
    global send
    pkt = b'\xAA' + b'\xFC' + uid + b'\x02'
    pkt = pkt + intToBytes(BCH(pkt))
    print(pkt)
    send.sendto(pkt, (targetIP, targetPort))

def receivePkt():
    global t1
    global conn
    global currRTU
    print("waiting for pkts")
    start = time.time()
    print(start)
    while True:
        #print(time.time())
        global recieve
        
        data, address = recieve.recvfrom(1024)
        #print(address)
        print("test")
        #print(data)
        if data[0] == 0xAA and data[1] == 0xFC:
            print("pkt received!")
            if len(data) == 5:
                currRTU = data[2]
            else:
                parsePkt(data)
        elif data[0] == 0xAA and data[1] == 0xFA:
            print("DPS pkt received!")
            if len(data) == 5:
                currRTU = data[2]
            else:
                parseDpsPkt(data)
        
        #print("waiting")
    print("closing connection")
    conn.commit()
    conn.close()


def twoC(val):
    if(val &(1 << (7))) != 0:
        val = val - (1 << 8)
    return val

def parsePkt(data):
    global conn
    global cur    
    
    print(currRTU)
    print(data[4])
	
    cur.execute("UPDATE devices SET tempAlrm=?, humAlrm=?, humid=?, temp=?, active = 1 WHERE id=?", (data[5], data[6], data[4], twoC(int(data[3])), currRTU))
    conn.commit()


    if(data[5] != 3):
        desc = ""
        if(data[5] == 1):
            desc = "Temp Major Under"
        elif(data[5] == 2):
            desc = "Temp Minor Under"
        elif(data[5] == 4):
            desc = "Temp Minor Over"
        else:
            desc = "Temp Major Over"
        cur.execute("SELECT name, ip FROM devices WHERE id=?", (currRTU,))
        result = cur.fetchall()
        devName = result[0][0]
        devIp =  result[0][1]
        cur.execute("SELECT id,description FROM standingAlarms WHERE id=? AND description=?", (currRTU, desc,))
        cur.fetchall()
        row_count = cur.rowcount
        if row_count == 0:          
            cur.execute("DELETE FROM standingAlarms WHERE id=? AND algType=?", (currRTU, "temp"))
            cur.execute("INSERT INTO standingAlarms (device,id,timestamp,description,type,ip,almCode,algType) VALUES(?,?,?,?,?,?,?,?)", (devName, currRTU, now, desc, "ALM", devIP, data[5], "temp"))
            conn.commit()
            cur.execute("INSERT INTO eventHist (device,id,timestamp,description,type,ip) VALUES(?,?,?,?,?,?)", (devName, currRTU, now, desc, "ALM", devIP))
            conn.commit()
    
    if(data[6] != 3):
        desc = ""
        if(data[6] == 1):
            desc = "Humid Major Under"
        elif(data[6]) ==  2:
            desc = "Humid Minor Under"
        elif(data[6]) == 4:
            desc = "Humid Minor Over"
        else:
            desc = "Humid Major Over"
        cur.execute("SELECT name, ip FROM devices WHERE id=?", (currRTU,))
        result=cur.fetchall()
        #print(result[0][0])
        devName = result[0][0]
        #devName = devName['name']
        devIp = result[0][1]
        #devIp = devIp['ip']

        cur.execute("SELECT id, description FROM standingAlarms WHERE id=? AND description=?", (currRTU, desc,))
        cur.fetchall()
        row_count = cur.rowcount
        print(row_count)
        if row_count == 0:
            cur.execute("DELETE FROM standingAlarms WHERE id=? AND algType=?", (currRTU, "hum"))
            cur.execute("INSERT INTO standingAlarms (device,id,timestamp,description,type,ip,almCode) VALUES (?, ?, ?, ?, ?, ?, ?)", (devName, currRTU, now, desc, "ALM", devIp, data[6],"hum"))
            conn.commit()
            cur.execute("INSERT INTO eventHist (device,id,timestamp,description,type,ip) VALUES (?, ?, ?, ?, ?, ?)", (devName, currRTU, now, desc, "ALM", devIp))
            conn.commit()    
        cur.execute("SELECT id, algType, almCode FROM standingAlarms WHERE id=? AND description=?", (currRTU, desc,))

def parseDpsPkt(data):
    if data[2] == 15:
        val1 = data[5] 
        val2 = data[6]
        val1 = val1 << 8
        #print(hex1)
        val1 = val1 | val2
        #print(hex1)
        cur.execute("UPDATE devices SET tempAlrm=?, temp=? WHERE id=?", (data[3], twoC(int(val1)), currRTU))
        conn.commit()

cur.execute("SELECT ip, port, id FROM devices")
result=cur.fetchall()
for (ip, port, id) in result:
    uIp=ip
    uPort=int(port)
    uId=id
    sendPkt(uIp, uPort, uId)

cur.execute("UPDATE devices SET active=0")
conn.commit()

start_new_thread(receivePkt,())
#start = time.time()
#print(start)
time.sleep(10)
quit