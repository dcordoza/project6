<?php
include('session.php');
?>
<?php
include('nav.php');
?>
<div class="topnav">
  <a href="stand.php" style="padding: 20;">Standing Alarms</a>
  <a href="devices.php" style="padding: 20;">Devices</a>
  <a href="eventHist.php" style="padding: 20;">Event History</a>
  <a style="padding: 20;" id="active">Analogs</a>
  <a href="logout.php" style="padding: 20;">Logout</a>
</div>
<script type="text/javascript" src="/js/gauge.js"></script>
<div class="row" id="createRow">
	<button id="createButton" onClick="hideForm()">
            Add Analog
    </button>
	<form id="gaugeForm" style="display:none;">
		<input type="text" id="dName" placeholder="Analog Name"><br>
		<input type="text" id="dID" placeholder="Device ID"><br>
		<input type="radio" id="dTemp" name="aType" value="dTemp">
  		<label>Temperature</label><br>
  		<input type="radio" id="dHum" name="aType" value="dHum">
		<label>Humidity</label><br>
		<input type="button" id="dSubmit" value="Submit" onClick="createModule()">
	</form>
	<p id="moduleForm"></p>
</div>
<div class="row">
  <!--<div class="rightcolumn">
    <div class="card">
      <h3>Popular Post</h3>
      <div id="Alrm"><p>Major Over</p></div>
      <div class="fakeimg"><p>Image</p></div>
      <div class="fakeimg"><p>Image</p></div>
    </div>
    <div class="card">
      <h3>Follow Me</h3>
      <p>Some text..</p>
    </div>
  </div>-->
  <div id="moduleList" class="leftcolumn">
  </div>
</div>

</body>
<script type="text/javascript">
  var content = localStorage.getItem("moduleList");
  document.getElementsByTagName('moduleList').innerHTML = content;
  var gaugeList = [];
  var gauges= [];
  var ids = [];
  var readList = [];
  var moduleList = [];
  class Module {
	  constructor(gauge, id, read, isTemp, module, active) {
		this.gauge = gauge;
		this.id = id;
		this.read = read;
		this.isTemp = isTemp;
		this.module = module;
		this.active = active;
	  }
  }
  //initGauge();
    var tempOpts = {
	  angle: 0.15, // The span of the gauge arc
	  lineWidth: 0.44, // The line thickness
	  radiusScale: 1, // Relative radius
	  pointer: {
		length: 0.6, // // Relative to gauge radius
		strokeWidth: 0.035, // The thickness
		color: '#000000' // Fill color
	  },
	  limitMax: false,     // If false, max value increases automatically if value > maxValue
	  limitMin: false,     // If true, the min value of the gauge will be fixed
	  colorStart: '#6FADCF',   // Colors
	  colorStop: '#8FC0DA',    // just experiment with them
	  strokeColor: '#E0E0E0',  // to see which ones work best for you
	  generateGradient: true,
	  highDpiSupport: true,     // High resolution support
	  
	  // static labels
		staticLabels: {
		  font: "10px sans-serif",
		  labels: [10, 22, 26, 32],
		  fractionDigits: 0
		},
	  
	  staticZones: [
		  {strokeStyle: "#c03eef", min: -10, max: 10},
		  {strokeStyle: "#3e76ef", min: 10, max: 22},
		  {strokeStyle: "#30B32D", min: 22, max: 26},
		  {strokeStyle: "#FFDD00", min: 26, max: 32},
		  {strokeStyle: "#F03E3E", min: 32, max: 37}
		],
	  
	};
	
	var humOpts = {
	  angle: 0.15, // The span of the gauge arc
	  lineWidth: 0.44, // The line thickness
	  radiusScale: 1, // Relative radius
	  pointer: {
		length: 0.6, // // Relative to gauge radius
		strokeWidth: 0.035, // The thickness
		color: '#000000' // Fill color
	  },
	  limitMax: false,     // If false, max value increases automatically if value > maxValue
	  limitMin: false,     // If true, the min value of the gauge will be fixed
	  colorStart: '#6FADCF',   // Colors
	  colorStop: '#8FC0DA',    // just experiment with them
	  strokeColor: '#E0E0E0',  // to see which ones work best for you
	  generateGradient: true,
	  highDpiSupport: true,     // High resolution support
	  
	  // static labels
		staticLabels: {
		  font: "10px sans-serif",
		  labels: [20, 40, 60, 80],
		  fractionDigits: 0
		},
	  
	  staticZones: [
		  {strokeStyle: "#c03eef", min: 0, max: 20},
		  {strokeStyle: "#3e76ef", min: 20, max: 40},
		  {strokeStyle: "#30B32D", min: 40, max: 60},
		  {strokeStyle: "#FFDD00", min: 60, max: 80},
		  {strokeStyle: "#F03E3E", min: 80, max: 100}
		],
	  
	};

  function initGauge(canvasId, readId, devId, isTemp, moduleId){
	  
	  var target = document.getElementById(canvasId); // your canvas element
	  console.log('target: ', target)
	  var gauge = new Gauge(target)
	  if(isTemp){
		gauge.setOptions(tempOpts); // create sexy gauge!
		gauge.maxValue = 37;
		gauge.setMinValue(-10);		
	  }else{
		gauge.setOptions(humOpts); // create sexy gauge!
		gauge.maxValue = 100;
		gauge.setMinValue(0);		
	  }
	  gauge.animationSpeed = 32; // set animation speed (32 is default value)
	  gauge.setTextField(document.getElementById(readId));
	  let module = new Module(gauge, devId, readId, isTemp, moduleId, 1);
	  moduleList.push(module);
	  console.log(moduleList);
	  //gauges.push(gauge);
	  //for(let i = 0; i < gauges.length; i++){
		//gauges[i].setOptions(opts);
	  //}

  }
  
  function removeGauge(id) {
    var elem = document.getElementById(id);
    elem.parentNode.removeChild(elem);
	var index = moduleList.indexOf(Module.read);
	for(let i = 0; i < moduleList.length; i++){
		if(moduleList[i].module == id){
			moduleList.splice(index, 1);
			return;
		}
	}
    //return false;
  }
  
  function hideForm(){
      var f = document.getElementById('gaugeForm');
      if(f.style.display == "none"){
		  f.style.display = "block";
		  return;
	  }else{
		 f.style.display = "none"; 
	  }
  }
  
  function createModule(){
	 var name = document.getElementById('dName').value;
	 var module = document.getElementById('dID').value + "Module";
	 var id = document.getElementById('dID').value;
	 var read = document.getElementById('dID').value + "Read";
	 console.log(id);
	 var tempRead;
	 if(document.getElementById("dTemp").checked == true){
		 tempRead=true;
		 read += "Temp";
		 module += "Temp";
	 }else{
		 tempRead = false;
		 module += "Hum";
	 }
	 
	 //var list = document.getElementById('moduleList').innerHTML;
	 /*list += `
			
			<div class="gaugeModule" id="${module}">
				<input type = "button" onClick = "removeGauge('${module}')" value = "X"/>
				<h2>${name}</h2>
				<p class="gaugeID">ID: ${id}</p>
				<div class="fakeimg">
					<canvas id="${name}"></canvas>
					<p class="gaugeRead"><span id="${read}"></span><span>°F</span></p>
				</div>
			</div>
			 `*/
	let mod = document.createElement("div");
		mod.setAttribute('class', 'gaugeModule');
		mod.setAttribute('id', module);
		let close = document.createElement("input");
			close.setAttribute('type', 'button');
			close.setAttribute('onClick', "removeGauge('"+module+"')");
			close.setAttribute('value', 'X');
		let modName = document.createElement("h2");
			let namet = document.createTextNode(name);
			modName.appendChild(namet);
		let modId = document.createElement("p");
			modId.setAttribute('class', 'gaugeID');
			let idt = document.createTextNode('ID: '+id);
			modId.appendChild(idt);
		let gaugeDiv = document.createElement("div");
			gaugeDiv.setAttribute('class', 'fakeimg');
			let gaugeCanvas = document.createElement("canvas");
			gaugeCanvas.setAttribute('id', name);
			let gaugeRead = document.createElement("p");
			gaugeRead.setAttribute('class', 'gaugeRead');
				let readSpan = document.createElement("span");
				readSpan.setAttribute('id', read);
				let unitSpan = document.createElement("span");
					let unitT
					if(tempRead){unitT = document.createTextNode('°C');}
					else{unitT = document.createTextNode('%RH');}
					unitSpan.appendChild(unitT);
				gaugeRead.appendChild(readSpan);
				gaugeRead.appendChild(unitSpan);
			gaugeDiv.appendChild(gaugeCanvas);
			gaugeDiv.appendChild(gaugeRead);
		
		mod.appendChild(close);
		mod.appendChild(modName);
		mod.appendChild(modId);
		mod.appendChild(gaugeDiv);
	document.getElementById("moduleList").appendChild(mod);
	
	
	
	/*gaugeList.push(module);
	ids.push(id);
	console.log(ids);
	console.log(gaugeList);
	console.log(gauges);*/
	//readList.push(read);
	initGauge(name, read, id, tempRead, module);
	document.getElementById('dName').value = "";
	document.getElementById('dID').value = "";
	document.getElementById("dTemp").checked = false;
	document.getElementById("dHum").checked = false;
	hideForm();
  }
  
 function updateGauges(){
        for(let i = 0; i < moduleList.length; i++){  
                //set value of each gauge
			//console.log(gaugeList[i]);
			//var lDown = document.getElementById(gaugeList[i]).getElementsByTagName("h1");
			let xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function(){
				if(xhttp.readyState == 4){
					let data = JSON.parse(xhttp.response);
					if(moduleList[i].isTemp==true){
						console.log(data.temp);
						moduleList[i].gauge.set(data.temp);
						console.log(data.active)
						moduleList[i].active = data.active;
					}else{
						console.log(data.humid);
						moduleList[i].gauge.set(data.humid);
						console.log(data.active)
						moduleList[i].active = data.active;
					}
				}
			};
			console.log("getValue.php?id="+moduleList[i].id);
			xhttp.open("get", "getValue.php?id="+moduleList[i].id);
			xhttp.send();
			console.log(active);
			if(moduleList[i].active == 0){
				//console.log(document.getElementById(gaugeList[i]).getElementsByTagName('h1').style.display);
				document.getElementById(moduleList[i].module).style.opacity = "0.5";
				console.log("device inactive")
			}else{
				//console.log(document.getElementById(gaugeList[i]).getElementsByTagName('h1').style.display)
				document.getElementById(moduleList[i].module).style.opacity = "1";
				console.log("device active")
			}

        }
  } 
  updateGauges();
  setInterval(function(){
	updateGauges()
  }, 5000);
  localStorage.setItem("moduleList", document.getElementById("moduleList").innerHTML);
</script>
</html>
