<html>
<head>
<style>
* {
  box-sizing: border-box;
}

.gaugeID {
  text-align: center;
  color: white;
  font-weight: bold;
  height: 28px;
  width: 100%;
  padding-top: 5px;
  text-decoration: none;
  background-color: #aaaaa9;
  border-radius: 8px;
  border: 0;
}

.gaugeModule h2{
  text-align: center;
}


body {
  font-family: Arial;
  padding: 10px;
  background: #f1f1f1;
}

/* Header/Blog Title */
.header {
  padding: 10px;
  text-align: center;
  background: white;
}

.header h1 {
  font-size: 50px;
  margin: 0
}

.header p {
  font-size: 30px;
  margin: 0
}

/* Style the top navigation bar */
.topnav {
  overflow: hidden;
  background-color: #000080;
}

/* Style the topnav links */
.topnav a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  /*text-decoration: none;*/
}

/* Change color on hover */
.topnav a:hover {
  background-color: #ddd;
  color: black;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
  float: left;
  width: 100%;
}

#tempRead{
  color: white;
  font-weight: bold;
  font-size: 200%;
  margin: 10px;
}

#humRead{
  color: white;
  font-weight: bold;
  font-size: 150%;
  margin: 10px;
}

#Alrm1 {
  border-radius: 25px;
  background: #73AD21;
  width: 110px;
  height: 50px; 
  text-align: center;
  line-height: 3;
  font-family: Arial;
  color: white;
  font-weight: bold;
  margin: 0 auto;
}

#Alrm2 {
  border-radius: 25px;
  background: #73AD21;
  width: 110px;
  height: 50px; 
  text-align: center;
  line-height: 3;
  font-family: Arial;
  color: white;
  font-weight: bold;
  margin: 0 auto;
}

#Alrm3 {
  border-radius: 25px;
  background: #73AD21;
  width: 110px;
  height: 50px; 
  text-align: center;
  line-height: 3;
  font-family: Arial;
  color: white;
  font-weight: bold;
  margin: 0 auto;
}

#Alrm4 {
  border-radius: 25px;
  background: #73AD21;
  width: 110px;
  height: 50px; 
  text-align: center;
  line-height: 3;
  font-family: Arial;
  color: white;
  font-weight: bold;
  margin: 0 auto;
}

/* Right column */
.rightcolumn {
  float: left;
  width: 25%;
  background-color: #f1f1f1;
  padding-left: 20px;
  padding-right: 20px;
}

/* Fake image */
.fakeimg {
  background-color: #b3c9dd;
  width: 100%;
  padding: 20px;
}

.fakeimg p {
  text-align: center;
  margin-top:0px;
}

/* Add a card effect for articles */
.card {
  background-color: white;
  margin-top: 20px;
  padding: 20px;
}

.temp {
  /*opacity: .5;*/
  background-color: white;
  margin-top: 20px;
  margin-left: 20px;
  padding: 20px;
  width: 250px;
  float: left;
}

.temp h2 {
  margin: 0px;
  color: white;
  background-color: grey;
  padding: 0px;
  text-align: center;
}

.card h2 {
  margin: 0px;
  color: white;
  background-color: grey;
  padding: 0px;
  text-align: center;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Footer */
.footer {
  padding: 20px;
  background: #ddd;
  margin-top: 20px;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
  .leftcolumn, .rightcolumn {   
    width: 100%;
    padding: 0;
  }
}

/* Responsive layout - when the screen is less than 400px wide, make the navigation links stack on top of each other instead of next to each other */
@media screen and (max-width: 400px) {
  .topnav a {
    float: none;
    width: 100%;
  }
}

table, td, th {  
  background-color: white;
  border: 1px solid #ddd;
  text-align: left;
}

table {
  border-collapse: collapse;
  width: 50%;
  margin-left: auto;
  margin-right: auto;
}

th, td {
  padding: 15px;
}

.gaugeRead {
	text-align: center;
	font-size: 18pt;
	font-weight: bold;
}

#readDemo {
	text-align: center;
	font-weight: bold;
}

.gaugeModule {
  background-color: white;
  margin-top: 20px;
  margin-left: 20px;
  padding: 20px;
  width: 380px;
  float: left;
}
</style>
</head>
<body>

<div class="header">
  <h1>Webserver</h1>
  <p>TempMonitor™ v1.0</p>
</div>

<div class="topnav">
  <a href="welcome.php" style="padding: 20;">Home</a>
  <a href="devices.php" style="padding: 20;">Devices</a>
  <a href="stand.php" style="padding: 20;">Standing Alarms</a>
  <a href="eventHist.php" style="padding: 20;">Event History</a>
  <a href="monitor.php" style="padding: 20;">Analogs</a>
  <a href="logout.php" style="padding: 20;">Logout</a>
</div>
